from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'profilepage/index.html', {'title': "Alif's Homepage"})

def bio(request):
    return render(request, 'profilepage/biography.html', {'title': "Biography"})

def contact(request):
    return render(request, 'profilepage/contact.html', {'title': 'Contact'})

def education(request):
    return render(request, 'profilepage/education.html', {'title': 'Education'})

def experience(request):
    return render(request, 'profilepage/experience.html', {'title': 'Experience'})

def skills(request):
    return render(request, 'profilepage/skills.html', {'title': 'Skills'})
